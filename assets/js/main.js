$(function () {
  $(document).scroll(function () {
    var $nav = $(".navbar");
    if ($(this).scrollTop() > $nav.height()) {
      $nav.addClass("scrolled");
      $nav.removeClass("navbar-dark");
      $nav.removeClass("navbar-light");
    } else {
      $nav.removeClass("scrolled");
      $nav.addClass("navbar-dark");
      $nav.removeClass("navbar-light");
    }
  });
  document.addEventListener('DOMContentLoaded', function () {
    // Mock data (replace this with your actual data)
    const items = [
        { name: 'Item 1', date: '2023-01-01' },
        { name: 'Item 2', date: '2023-02-01' },
        // Add more items as needed
    ];

    const contentContainer = document.getElementById('content');
    const sortSelect = document.getElementById('sort');
    const showPerPageSelect = document.getElementById('showPerPage');

    let currentPage = 1;
    let itemsPerPage = parseInt(showPerPageSelect.value);

    // Function to render items on the page
    function renderItems() {
        // Clear the content container
        contentContainer.innerHTML = '';

        // Sort the items based on the selected option
        const sortedItems = sortItems(items, sortSelect.value);

        // Calculate the start and end index of items to display on the current page
        const startIndex = (currentPage - 1) * itemsPerPage;
        const endIndex = startIndex + itemsPerPage;

        // Slice the items array to get the items for the current page
        const itemsToShow = sortedItems.slice(startIndex, endIndex);

        // Render each item
        itemsToShow.forEach(item => {
            const itemElement = document.createElement('div');
            itemElement.classList.add('item');
            itemElement.textContent = `${item.name} - ${item.date}`;
            contentContainer.appendChild(itemElement);
        });
    }

    // Function to sort items based on the selected option
    function sortItems(items, sortBy) {
        return items.sort((a, b) => {
            if (sortBy === 'name') {
                return a.name.localeCompare(b.name);
            } else if (sortBy === 'date') {
                return new Date(a.date) - new Date(b.date);
            }
            // Add more sorting options as needed
        });
    }

    // Event listener for sort select
    sortSelect.addEventListener('change', function () {
        renderItems();
    });

    // Event listener for show-per-page select
    showPerPageSelect.addEventListener('change', function () {
        itemsPerPage = parseInt(showPerPageSelect.value);
        renderItems();
    });

    // Initial render
    renderItems();
});

});
